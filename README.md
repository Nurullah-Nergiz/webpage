# webpage

Frontend portfolio application is the platform where I showcase my work and demonstrate my technical skills, and create user-friendly and interactive web interfaces.

---

## Download

```bash
git clone git+https://github.com/Nurullah-Nergiz/webpage.git
```

Recommended NodeJS Version [v21.2.0](https://nodejs.org/dist/v21.2.0)

## Dependencies

	axios: ^1.3.3
	markdown-html-transformer: ^1.0.3
	
## DevDependencies

	@nuxtjs/tailwindcss: ^6.8.0
	nuxt: 3.5.0
	nuxt-simple-robots: ^2.3.0
	nuxt-simple-sitemap: ^2.7.0
	

---

## Author

 - Nurullah Nergiz

- ![Website](https://img.shields.io/website?url=https://nurullahnergiz.com/&up_message=visit&up_color=%23fff&link=https://nurullahnergiz.com/)

- [![GitHub](https://img.shields.io/badge/GitHub-000000?style=for-the-badge&logo=github&logoColor=white)](https://www.github.com/Nurullah-Nergiz)

- [![Twitter](https://img.shields.io/badge/Twitter-%231DA1F2.svg?logo=Twitter&logoColor=white)](https://twitter.com/nurullahNergiz_)

- [![LinkedIn](https://img.shields.io/badge/LinkedIn-%230077B5.svg?logo=linkedin&logoColor=white)](https://linkedin.com/in/nurullah-nergiz)

- [![Medium](https://img.shields.io/badge/Medium-12100E?logo=medium&logoColor=white)](https://medium.com/@nurullahnergiz)


---

## License

![GitHub License](https://img.shields.io/github/license/Nurullah-Nergiz/webpage?style=social&logo=github&label=License)

This project was generated with [Docify-Cli](https://www.npmjs.com/package/docify-cli).