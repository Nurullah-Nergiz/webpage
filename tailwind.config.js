/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  theme: {
    extend: {
      colors: {
        primary: "#d81f26",
        secondary: "rgba(255, 255, 255, 0.05)",
        third: "rgba(255, 255, 255, 0.12)",
      },
    },
  },
  plugins: [],
};
